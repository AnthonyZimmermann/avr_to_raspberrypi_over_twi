#include "twi.h"
#include <util/delay.h>

int main()
{
    char* s = "abcdefghijklmnopqrstuvwxyz";

    while(1)
    {
        // 8MHz cpu clock -> prescaler 2, bitrate 255 -> ~1kHz SCL
        // cpu/ (16 + (2*bitrate)*(4**prescaler))
        twi_init(2, 255);
        twi_write_data(0x44,(uint8_t*)s,26);
        _delay_ms(2000);
    }
}
